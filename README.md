
[<button name="button" onclick="https://gitlab.inria.fr/moex/ontosim">[Source](https://gitlab.inria.fr/moex/ontosim)</button>]
[<button name="button" onclick="https://moex.inria.fr">[mOeX](https://moex.inria.fr)</button>]


# OntoSim

OntoSim is a Java API allowing to compute similarities between ontologies.
It relies on the Alignment API for ontology loading so it is quite independent of the ontology API used (JENA or OWL API).

OntoSim provides a framework for designing various kinds of similarities. 
In particular, we differentiated similarities in the ontology space [[David2008a](https://bibexmo.inrialpes.fr/BibServ/BibrefServlet?file=bibexmo.xml&abstrip=false&format=html&ref=david2008a)] from those in the alignment space [[Euzenat2009b](https://bibexmo.inrialpes.fr/BibServ/BibrefServlet?file=bibexmo.xml&abstrip=false&format=html&ref=euzenat2009b),[David2010b](https://bibexmo.inrialpes.fr/BibServ/BibrefServlet?file=bibexmo.xml&abstrip=false&format=html&ref=david2010b)].
The latter ones make use of available alignments in a network of ontologies while the former only rely on ontology data. 

OntoSim is based on an ontology interface, shared with the [Alignment API](https://gitlab.inria.fr/moex/alignapi) [Ontowrap package](https://moex.gitlabpages.inria.fr/alignapi/ontowrap.html) for using ontology parsed with different APIs.

## Structure

<dt>string/</dt>
<dd>Measures for comparing strings, including the inclusion of [SecondString](http://secondstring.sourceforge.net/">http://secondstring.sourceforge.net/) measures</dd>
<dt>entity/</dt>
<dd>Measures for comparing ontology entities (OLA, triple-based, alignment-based, string-based)</dd>
<dt>vector/</dt>
<dd>Measure for comparing objects in a vector space (cosine, Jaccard, Kendall)</dd>
<dt>set/</dt>
<dd>Measures for comparing sets of objects (linkages, weighted sums)</dd>
<dt>aggregation/</dt><dd>Classes for aggregating several measures (Means)</dd>
<dt>align/</dt>
<dd>Measures for comparing ontologies in the alignment space
(e.g. largest coverage, union path coverage, agreement, disagreement).</dd>
<dt>extractor/</dt>
<dd>Coupling extractor from a similarity matrix (e.g., Maximum coupling, greedy algorithm, Hausdorff)</dd>
<dt>util/</dt>
<dd>utility classes for caching measures values on disk, storing efficiently large sparse matrix of double</dd>

## API

### The Measure interface

The OntoSim API is based on a very simple generic `Measure` interface. 
The `Measure` interface defines 3 main methods: `getMeasureValue`, `getSim`, `getDissim`.: 
~~~~
public interface Measure<O> {
	static enum TYPES {similarity, dissimilarity, distance, other};
	
	public TYPES getMType();
	public double getMeasureValue( O o1, O o2);
	public double getSim( O o1, O o2);
	public double getDissim( O o1, O o2);
}
~~~~

These 3 methods take in parameter the two objects to compare and return a double value.

This generic interface is instanciated for defining the profile of various basic categories of measures:
<dt>String measures</dt><dd><tt>O=String</tt></dd>
<dt>Entity measures</dt><dd><tt>O=Entity<?></tt></dd>
<dt>Set measures</dt><dd><tt>O=Set<? extends S></tt></dd>
<dt>Vector measures</dt><dd><tt>O=double[]</tt></dd>

### MatrixMeasure

The `MatrixMeasure` interface adds 3 other methods to the `Measure` interface (`getMeasureValue`, `getSim`, `getDissim`) which take two sets of objects and return a `Matrix` object or `MatrixDoubleArray` (already defined in OntoSim). 
The `AbstractMatrixMeasure` implements all the additional methods so that implementing it, can be reduced to defining the `O` parameter of the `Measure` interface (entity, string, etc.). 

This allows to define the `SetMeasure` interface parametrised by:
* a *local `Measure`*;
* an *`Extractor`:* Thresholding, MWGM, Stable Marriage, max (for FullLinkage), min (for SingleLinkage), max-min for Hausdorff, etc. Some set measures, i.e. AverageLinkage, do not need to use an Extractor.
* an *`AggregationScheme`:* some average (arithmetic, geometric, harmonic, etc. means), weighted sum, etc. Some measures, those which use only one value such as FullLinkage,SingleLinkage or Hausdorff, do not need Aggregation Scheme.

### Extractor

The `Extractor` interface regroups both alignement extractor and filtering notions used by matching algorithms. 
It is the basis for implementing MWGM, StableMariage, etc. (all methods extract; defined in `DistanceAlignment`).
The interface is:
~~~~
public interface Extractor {
   Cardinality getCardinality();
   Object[][] extract(Matrix m);
   //or
   Matching extract(Matrix m);
}
~~~~
The `getCardinality()` method returns the cardinality of the produced matching (1-1, n-n, etc). 
The `Matching` object is a kind  of lighter alignment object.

### AggregationScheme

The `AggregationScheme` interface has two implementing classes: one for averages and another one for weighted sums.
~~~~
public  AggregationScheme {
    double getValue(double[] vals);
}
~~~~
With the new version of OntoSim, the `DistanceAlignment` class should be refactored in order to easily use OntoSim entity measures.

~~~~java
public abstract class AggregationScheme {
    public abstract double getValue(double[] vals);
    public abstract <O> double getValue(Measure<O> measure,Matching<O> matching);
}
~~~~

### Ontology measures

From this it is possible to define general classes of measures such as:
~~~~bash
Measure
|- AlignmentSpaceMeasure (abstract: OntologyNetwork)
|- OntologySpaceMeasure (abstract: )
|- VectorSpaceMeasure (abstract: Collection<LoadedOntology>)
~~~~
These measures are simply defined with respect to their internal variables.

## Example

~~~~java
OntologyFactory of=OntologyFactory.getFactory();
LoadedOntology o1 = of.loadOntology(uri1);
LoadedOntology o2 = of.loadOntology(uri2);
Vector<LoadedOntology> ontos = new Vector<LoadedOntology>();
ontos.add(o1);
ontos.add(o2);

VectorLexicalSim m = new VectorLexicalSim(ontos,new JaccardVM(), DocumentCollection.VECTOR_TYPE.TFIDF);
System.out.println(m.getSim(o1, o2,));

SetMeasure<Entity> cm = new MaxCoupling(new OLAEntitySim());
GeneralOntologyMeasure m2 = new GeneralOntologyMeasure(cm);
System.out.println(m2.getSim(o1, o2,));

EntityLexicalMeasure lm=new EntityLexicalMeasure(new StringMeasureSS(new Levenstein()));
SetMeasure<Entity>  cm2 = new MaxCoupling(lm);
GeneralOntologyMeasure m3 = new GeneralOntologyMeasure(cm2);
System.out.println(m3.getSim(o1, o2));
~~~~

## License

OntoSim is available under the [LGPL 2.1](https://www.gnu.org/licenses/lgpl-2.1.html) or above.

## Command-line use

Ontosim is not supposed to be used at command line.
However, there is a batch structure:
```bash
$ JAVALIB=${CLASSPATH}:lib/ontosim.jar:lib/alignapi/align.jar:lib/alignapi/ontowrap.jar:lib/alignapi/procalign.jar:lib/getopt/getopt.jar:lib/jena/arq.jar:lib/jena/icu4j.jar:lib/jena/iri.jar:lib/jena/jena.jar:lib/jwnl/jwnl.jar:lib/log4j/commons-logging.jar:lib/log4j/log4j.jar:lib/log4j/slf4j-api.jar:lib/log4j/slf4j-log4j.jar:lib/lucene/lucene-analyzers-3.0.2.jar:lib/lucene/lucene-core-3.0.2.jar:lib/lucene/lucene-snowball-3.0.2.jar:lib/ola/mtj.jar:lib/ola/ontographs.jar:lib/ola/sboa.jar:lib/owlapi10/api.jar:lib/owlapi10/impl.jar:lib/owlapi10/io.jar:lib/owlapi10/rdfapi.jar:lib/owlapi10/rdfparser.jar:lib/owlapi3/owlapi-bin.jar:lib/secondstring/secondstring-20060615.jar:lib/testng/testng.jar:lib/xerces/xercesImpl.jar
$ java -cp ${JAVALIB} fr.inrialpes.exmo.ontosim.util.BatchMeasures -a align onto measure
```

----

[https://gitlab.inria.fr/moex/ontosim](https://gitlab.inria.fr/moex/ontosim)

<small>
</small>
